import java.util.Scanner;

public class Sphere {

    public static void main(String[] args) {
//        System.out.println("Enter a radius:");
//        Scanner scanner = new Scanner(System.in);
//        int radius;
//
//        while (!scanner.hasNextDouble()) {
//            System.out.println("Enter a number!");
//            scanner.next();
//        }
//        radius = scanner.nextInt();
//        System.out.println("Volume of sphere with radius = " + radius + " is equal to: " + calculateVolume(radius));
    }

    public double calculateVolume(int radius) {
        double v = (4.0 / 3.0) * Math.PI * (radius * radius * radius);
        return Math.round(v * 100.0) / 100.0;
    }
}
