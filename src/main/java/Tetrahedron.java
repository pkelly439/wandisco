import java.util.Scanner;

public class Tetrahedron {

    public static void main(String[] args) {
//        System.out.println("Enter a side length:");
//
//        Scanner scanner = new Scanner(System.in);
//        double length;
//        while (!scanner.hasNextDouble()) {
//            System.out.println("Enter a number!");
//            scanner.next();
//        }
//        length = scanner.nextDouble();
//
//        System.out.println("Volume of tetrahedron with side = " + length + " is equal to: " + calculateVolume(length));
    }

    public double calculateVolume(int length) {
        double cubed = length * length * length;
        double bottomLine = 6 * Math.sqrt(2);

        return Math.round((cubed / bottomLine) * 100.0) / 100.0;
    }
}
