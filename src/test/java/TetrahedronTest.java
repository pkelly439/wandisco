import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

public class TetrahedronTest {
    @Test
    public void tetraTestOne() {
        Tetrahedron t = new Tetrahedron();
        double vol = t.calculateVolume(1);

        Assert.assertThat(vol, CoreMatchers.is(0.12));
    }

    @Test
    public void tetraTestTwo() {
        Tetrahedron t = new Tetrahedron();
        double vol = t.calculateVolume(5);

        Assert.assertThat(vol, CoreMatchers.equalTo(14.73));
    }
}
