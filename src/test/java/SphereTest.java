import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

public class SphereTest {
    @Test
    public void sphereTestOne() {
        Sphere s = new Sphere();
        double volume = s.calculateVolume(1);

        Assert.assertThat(volume, CoreMatchers.is(4.19));
    }

    @Test
    public void sphereTestTwo() {
        Sphere s = new Sphere();
        double volume = s.calculateVolume(5);

        Assert.assertThat(volume, CoreMatchers.is(523.6));
    }
}
